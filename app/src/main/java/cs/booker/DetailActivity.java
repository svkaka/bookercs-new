package cs.booker;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DetailActivity extends Activity {

    TextView token, request, stav, datum, urad, adresa, spec, hodiny;
    ImageButton imageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Intent intent=getIntent();
        String tokenS = intent.getStringExtra("Token");
        String requestS = intent.getStringExtra("Request");
        int stavI = intent.getIntExtra("State", 2);
        String stavS;
        switch (stavI) {
            case 0:
                stavS = "nepotvrdená";
                break;
            case 1:
                stavS = "vypršala";
                break;
            case 2:
                stavS = "potvrdená";
                break;
            case 3:
                stavS = "zrušená";
                break;
            default:
                stavS = "potvrdená";
                break;
        }
        String datumS = intent.getStringExtra("Date");
        String casS = intent.getStringExtra("Time");
        String uradS = intent.getStringExtra("Office");
        final String adresaS = intent.getStringExtra("Address");
        imageButton= (ImageButton) findViewById(R.id.image);
        token = (TextView)findViewById(R.id.detailToken);
        token.setText("Token: "+tokenS);
        request = (TextView)findViewById(R.id.detailRequest);
        request.setText("Vec: "+requestS);
        stav = (TextView)findViewById(R.id.detailStav);
        stav.setText("Stav: "+stavS);
        datum = (TextView)findViewById(R.id.detailDatumCas);
        datum.setText("Dátum: "+datumS+"        Čas: "+casS);
        urad = (TextView)findViewById(R.id.detailNazovUradu);
        urad.setText("Úrad: "+uradS);
        adresa = (TextView)findViewById(R.id.detailAdresa);
        adresa.setText("Adresa: "+adresaS);

        SQLiteDatabase mydatabase = openOrCreateDatabase(MainActivity.DATABASE_NAME, MODE_PRIVATE, null);
        Cursor res = mydatabase.rawQuery("SELECT * FROM "+MainActivity.OFFICES_TABLE+" WHERE "+MainActivity.URAD_NAME+" = ?; ", new String[] {uradS});
        res.moveToFirst();
        String hodinyS = res.getString(res.getColumnIndex(MainActivity.HOURS));
        String specS = res.getString(res.getColumnIndex(MainActivity.SPECIALIZATION));

        spec = (TextView)findViewById(R.id.detailSpecializacia);
        spec.setText("Špecializácia: "+specS);
        hodiny = (TextView)findViewById(R.id.detailHodiny);
        hodiny.setText("Úradné hodiny:\n"+hodinyS);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browser =new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.sk/maps/search/"+adresaS.replace(" ","%20")+"?hl=en&source=opensearch"));
            startActivity(browser);
            }
        });
    }
}
