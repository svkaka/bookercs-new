package cs.booker;

import java.util.Date;

/**
 * Created by PaFi on 30. 3. 2016.
 */
public class Reservation {
    private String problem;
    private Date time;
    private String location;
    private String token;
    private int state;
    private String office;



    public Reservation(String problem, Date time, String office, String location, String token, int state){
         this.setProblem(problem);
         this.setTime(time);
        this.setOffice(office);
        this.setLocation(location);
        this.setToken(token);
        this.setState(state);
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return getToken()+getProblem()+getLocation()+getTime()+getState();
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }
}
