package cs.booker.RestModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Veronika on 6. 5. 2016.
 */
public class Urad {

    private String UradId;
    private String Nazov;
    private String Specializacia;
    private String Popis;
    private String Adresa;
    private String Otv_hodiny;
    private String Self;
    private List<Objednavka> Objednavkas = new ArrayList<>();

    public String getUradId() {
        return UradId;
    }

    public String getNazov() {
        return Nazov;
    }

    public String getSpecializacia() {
        return Specializacia;
    }

    public String getPopis() {
        return Popis;
    }

    public String getAdresa() {
        return Adresa;
    }

    public String getOtv_hodiny() {
        return Otv_hodiny;
    }

    public String getSelf() {
        return Self;
    }

    public List<Objednavka> getObjednavkas() {
        return Objednavkas;
    }

    public class Objednavkas {
        private String ObjednavkaId;
        private String Token;
        private String Datum;
        private String DateTime;
        private String UradId;
        private String Self;
        private String Email;

        public Objednavkas(String id, String token, String datum, String email, String dateTime, String uradId, String self) {
            this.ObjednavkaId = id;
            this.Token = token;
            this.Datum = datum;
            this.Email = email;
            this.DateTime = dateTime;
            this.UradId = uradId;
            this.Self = self;
        }

        public String getObjednavkaId() {
            return ObjednavkaId;
        }

        public String getToken() {
            return Token;
        }

        public String getDatum() {
            return Datum;
        }

        public String getDateTime() {
            return DateTime;
        }

        public String getUradId() {
            return UradId;
        }

        public String getSelf() {
            return Self;
        }

        public String getEmail() {
            return Email;
        }
    }
}
