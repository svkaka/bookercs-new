package cs.booker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by PaFi on 30. 3. 2016.
 */
public class ListViewAdapter extends ArrayAdapter<Reservation> {
    private Context context;
    private int layoutResourceId;
    private ArrayList<Reservation> data = new ArrayList<>();
    public int pos;
    private static String setColor1="<font color='#EE0000'>";
    private static String setColor2="</font>";

    public ListViewAdapter(Context context, int layoutResourceId, ArrayList<Reservation> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        pos = position;
        Reservation item = data.get(position);

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
        } else {
            holder = (ViewHolder) row.getTag();
        }
        if (item != null) {
            holder.hlavne = (TextView) row.findViewById(R.id.textView);
            holder.popis = (TextView) row.findViewById(R.id.textView2);
            holder.datum = (TextView) row.findViewById(R.id.textView3);
            holder.cas = (TextView) row.findViewById(R.id.textView4);

            holder.nepo = (Button) row.findViewById(R.id.nepotvrdena);
            holder.potv = (Button) row.findViewById(R.id.potvrdena);
            holder.presla = (Button) row.findViewById(R.id.vyprsala);
            holder.zrus = (Button) row.findViewById(R.id.zrusena);
            DateFormat df=new SimpleDateFormat("dd.MM.yyyy");
            DateFormat df2=new SimpleDateFormat("HH:mm");

            String date=df.format(item.getTime());
            String time=df2.format(item.getTime());

            //String text = "<font color=#cc0029>Erste Farbe</font> <font color=#ffcc00>zweite Farbe</font>";
            // holder.hlavne.setText(Html.fromHtml(text));

            String main= "Token: "+setColor1+item.getToken()+setColor2+" ";//+item.getProblem();
            String note=time+"\n"+item.getOffice()+"\n"+item.getLocation();

           holder.hlavne.setText(Html.fromHtml(main));
            holder.popis.setText(item.getProblem());
            holder.datum.setText(date);
            holder.cas.setText(time);
            //System.out.println(item.getProblem()+" STATE of " +item.getState());
            switch (item.getState()) {
                case 1:
                    holder.nepo.setVisibility(View.VISIBLE);
                    holder.potv.setVisibility(View.GONE);
                    holder.presla.setVisibility(View.GONE);
                    holder.zrus.setVisibility(View.GONE);
                    break;

                case 2:
                    holder.nepo.setVisibility(View.GONE);
                    holder.potv.setVisibility(View.VISIBLE);
                    holder.presla.setVisibility(View.GONE);
                    holder.zrus.setVisibility(View.GONE);
                    break;
                case 3:
                    holder.nepo.setVisibility(View.GONE);
                    holder.potv.setVisibility(View.GONE);
                    holder.presla.setVisibility(View.VISIBLE);
                    holder.zrus.setVisibility(View.GONE);
                    break;
                case 4:
                    holder.nepo.setVisibility(View.GONE);
                    holder.potv.setVisibility(View.GONE);
                    holder.presla.setVisibility(View.GONE);
                    holder.zrus.setVisibility(View.VISIBLE);
                    break;
                default:
                    Toast.makeText(this.getContext(), "State unknown", Toast.LENGTH_LONG);
                    break;
            }

        }
        row.setTag(holder);

        return row;
    }

    static class ViewHolder {
        TextView hlavne;
        TextView popis;
        TextView datum;
        TextView cas;
        Button nepo;
        Button potv;
        Button presla;
        Button zrus;

    }
}