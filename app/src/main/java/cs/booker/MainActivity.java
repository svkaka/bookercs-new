package cs.booker;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import cs.booker.RestModel.Odpoved;
import cs.booker.RestModel.Objednavka;
import cs.booker.RestModel.Pouzivatel;
import cs.booker.RestModel.Urad;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends Activity {

    public static final String USER_EMAIL ="email" ;
    public static final String USER_REQUEST = "request";
    public static final String API_BASE_URL = "http://bookeraplication.azurewebsites.net/api/";
    public static final String RESERVATIONS_NUMBER = "reservations_number";
    public static final String DATABASE_NAME = "Databaza";
    public static final String RESERVATIONS_TABLE = "MojeObjednavky";
    public static final String OFFICES_TABLE = "Urady";
    public static final String TOKEN = "Token";
    public static final String REQUEST = "Request";
    public static final String DATETIME = "DateTime";
    public static final String URAD_ID = "UradId";
    public static final String URAD_NAME = "Nazov";
    public static final String STATE = "Stav";
    public static final String SPECIALIZATION = "Specializacia";
    public static final String INFO = "Popis";
    public static final String ADDRESS = "Adresa";
    public static final String HOURS = "OtvHodiny";
    public static final String RESERVATION_ID = "Reservation_id";
    public static final String REQUESTS_TABLE = "Requests";

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader = new ArrayList<>();
    HashMap<String, List<Problem>> listDataChild = new HashMap<>();
    List<Problem> nahlasenie = new ArrayList();
    List<Problem> odhlasenie = new ArrayList();
    List<Problem> ziadost = new ArrayList();
    List<Problem> podanie = new ArrayList();
    List<Problem> platenie = new ArrayList();
    List<Problem> udelenie = new ArrayList();
    List<Problem> vybavenie = new ArrayList();
    List<Problem> vydavanie = new ArrayList();
    List<Problem> poskytovanie = new ArrayList();
    Button rezer, back, calendar;
    EditText email;
    String chudacikPotrebuje="";
    RestApi myApi;
    int pocetObj, uradId;
    String userEmail = null, date, dateFromCalendar = "";
    SQLiteDatabase mydatabase;
    private GoogleApiClient mGoogleApiClient;
    SignInButton signInButton;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent=getIntent();
        userEmail=intent.getStringExtra(MainActivity.USER_EMAIL);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        expListView = (ExpandableListView) findViewById(R.id.expandableListView);
        prepareListData();
        rezer= (Button) findViewById(R.id.buttonRez);
        back = (Button) findViewById(R.id.buttonBack);
        //calendar = (Button) findViewById(R.id.calendar);
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        //email= (EditText) findViewById(R.id.editText);
        //if(userEmail != null) email.setText(userEmail);
        expListView.setAdapter(listAdapter);

        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        /*mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(MainActivity.this,"Connection Failed",Toast.LENGTH_LONG).show();
                        System.out.println("failed");
                    }
                } /* OnConnectionFailedListener *//*)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();

        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setScopes(gso.getScopeArray());
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.sign_in_button:
                        signIn();
                        break;
                    // ...
                }
            }
        });*/

        rezer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.dialog, null, false);
                final CalendarView cv = (CalendarView) view.findViewById(R.id.calendarView1);
                cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

                    @Override
                    public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                        dateFromCalendar = ""+year+" "+(month+1)+" "+dayOfMonth;
                    }
                });
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Výber dátumu")
                        .setView(view)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                nic2();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dateFromCalendar = "";
                            }
                        }



                ).show();
            }
        });

       /* Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);*/

        /*email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (chudacikPotrebuje.isEmpty()) {
                    rezer.setBackgroundColor(Color.argb(255,125,125,0));
                }else  rezer.setBackgroundColor(Color.argb(255,0,255,0));
                return true;
            }
        });*/
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ReservationsActivity.class);
                intent.putExtra(MainActivity.USER_EMAIL, userEmail);
                intent.putExtra(MainActivity.RESERVATIONS_NUMBER, pocetObj);
                startActivity(intent);
            }
        });

        expListView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                parent.setItemChecked(index, true);
                chudacikPotrebuje = expListView.getExpandableListAdapter().getChild(groupPosition, childPosition).toString();
                System.out.println("group "+groupPosition + " child " + childPosition);
                List<Problem> list = listDataChild.get(listDataHeader.get(groupPosition));
                System.out.println(list.get(childPosition).getUradId());
                uradId = list.get(childPosition).getUradId();
                return false;
            }
        });
        /*rezer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nic2();
            }
        });*/
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("MAIN", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            email.setText(getString(R.string.signed_in_fmt, acct.getEmail()));
            //updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            // updateUI(false);
        }
    }

    public void getRest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        myApi = retrofit.create(RestApi.class);
        //contactsCall();
        //officesCall();

    }

    public void officesCall() {
        pocetObj = 0;
        Call<List<Urad>> call = myApi.loadOffices();
        call.enqueue(new Callback<List<Urad>>() {
            @Override
            public void onResponse(Response<List<Urad>> response, Retrofit retrofit) {
                List<String> names = new ArrayList<>();
                for (int i = 0; i < response.body().size(); i++) {
                    String name = response.body().get(i).getNazov();
                    String id = response.body().get(i).getUradId();
                    Log.d("Id ", id);
                    Log.d("Nazov ", name);
                    names.add(name);
                    pocetObj += response.body().get(i).getObjednavkas().size();
                }
                Log.d("POCET objednavok ", "" + pocetObj);

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "Nepodarilo sa spojiť so serverom. Skontrolujte internetové pripojenie.", Toast.LENGTH_LONG).show();
                System.out.println(t.getLocalizedMessage());
            }
        });
    }

    public void contactsCall() {
        Call<List<Pouzivatel>> call = myApi.loadContacts();
        call.enqueue(new Callback<List<Pouzivatel>>() {
            @Override
            public void onResponse(Response<List<Pouzivatel>> response, Retrofit retrofit) {
                List<String> names = new ArrayList<>();
                for (int i = 0; i < response.body().size(); i++) {
                    String name = response.body().get(i).getName();
                    Log.d("Names", "" + name);
                    names.add(name);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "Nepodarilo sa spojiť so serverom. Skontrolujte internetové pripojenie.", Toast.LENGTH_LONG).show();
                System.out.println(t.getLocalizedMessage());
            }
        });
    }

    private void prepareDatabase() {
        mydatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        mydatabase.execSQL("DROP TABLE IF EXISTS "+RESERVATIONS_TABLE);
        mydatabase.execSQL("DROP TABLE IF EXISTS "+REQUESTS_TABLE);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS " + RESERVATIONS_TABLE + "(" + RESERVATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+ TOKEN + " VARCHAR, "+REQUEST+" VARCHAR,"
                + DATETIME + " VARCHAR," + URAD_ID + " INTEGER," + STATE + " INTEGER);");
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS " + OFFICES_TABLE + "(" + URAD_ID + " INTEGER, "+URAD_NAME+" VARCHAR,"
                + SPECIALIZATION +" VARCHAR," + INFO + " VARCHAR," + ADDRESS + " VARCHAR," + HOURS + " VARCHAR);");
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS " + REQUESTS_TABLE + "(" + RESERVATION_ID + " INTEGER, "
                +REQUEST+" VARCHAR);");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MainActivity.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // prepare call in Retrofit 2.0
        myApi = retrofit.create(RestApi.class);
        Call<List<Urad>> call = myApi.loadOffices();
        call.enqueue(new Callback<List<Urad>>() {
            @Override
            public void onResponse(Response<List<Urad>> response, Retrofit retrofit) {
                updateOfficesDbs(response);

            }

            @Override
            public void onFailure(Throwable t) {
                //Toast.makeText(MainActivity.this, "Chyba v internetovom pripojení. Údaje o úradoch boli načítané z lokálnej databázy.", Toast.LENGTH_LONG).show();
                loadOfficesFromDbs();
            }
        });
        /*res.moveToFirst();
        while(res.isAfterLast() == false){
            System.out.println(res.getString(res.getColumnIndex(REQUEST)));
            res.moveToNext();
        }*/
    }

    private void updateOfficesDbs(Response<List<Urad>> response) {
        mydatabase.execSQL("DROP TABLE IF EXISTS "+OFFICES_TABLE);
        mydatabase.execSQL("CREATE TABLE IF NOT EXISTS " + OFFICES_TABLE + "(" + URAD_ID + " INTEGER, "+URAD_NAME+" VARCHAR,"
                + SPECIALIZATION +" VARCHAR," + INFO + " VARCHAR," + ADDRESS + " VARCHAR," + HOURS + " VARCHAR);");
        for (int i = 0; i < response.body().size(); i++) {
            //for(int j = 0; j < response.body().get(i).getObjednavkas().size(); j++) {
            String nazov = response.body().get(i).getNazov();
            String spec = response.body().get(i).getSpecializacia();
            String popis = response.body().get(i).getPopis();
            String hodiny = response.body().get(i).getOtv_hodiny();
            String adresa = response.body().get(i).getAdresa();
            //String token = response.body().get(i).getObjednavkas().get(j).getToken();
            //String dateTime = response.body().get(i).getObjednavkas().get(j).getDateTime();
            int id = Integer.parseInt(response.body().get(i).getUradId());
            mydatabase.execSQL("INSERT INTO "+OFFICES_TABLE+" VALUES('"+id+"','"+nazov+"','"+spec+"','"+popis+"','"+adresa+"','"+hodiny+"');");
                        /*String mail = response.body().get(i).getObjednavkas().get(j).getEmail();
                        if(mail.equals(email.getText().toString())) {
                            mydatabase.execSQL("INSERT INTO "+ RESERVATIONS_TABLE +" VALUES('"+token+"','"
                                    +chudacikPotrebuje+"','"+dateTime+"','"+id+"','2');");
                            System.out.println("INSERT "+token);
                        }*/
            //}
        }
        Cursor res = mydatabase.rawQuery("SELECT * FROM " + OFFICES_TABLE, null);
        System.out.println("UpdateOfficesDBS - pocet Uradov " + res.getCount());
    }

    private void loadOfficesFromDbs() {
        mydatabase = openOrCreateDatabase(DATABASE_NAME,MODE_PRIVATE,null);
        Cursor res =  mydatabase.rawQuery( "SELECT * FROM "+OFFICES_TABLE, null);
        System.out.println("LoadFromDBS - pocet Uradov "+res.getCount());
        res.moveToFirst();

        while(res.isAfterLast() == false){
            //array_list.add(res.getString(res.getColumnIndex(TOKEN)));
            String nazov = res.getString(res.getColumnIndex(URAD_NAME));
            String spec = res.getString(res.getColumnIndex(SPECIALIZATION));
            String popis = res.getString(res.getColumnIndex(INFO));
            String adresa = res.getString(res.getColumnIndex(ADDRESS));
            String hodiny = res.getString(res.getColumnIndex(HOURS));
            int id = res.getInt(res.getColumnIndex(MainActivity.URAD_ID));
            System.out.println(nazov+", "+spec+", "+popis+", "+ adresa +", "+id+", "+hodiny);
            //myReservations.add(new Reservation(request, date, urad, adresaUradu, token, 2));
            res.moveToNext();
        }
    }

    public void nic2(){
    //final String mail = email.getText().toString();
        if (!(chudacikPotrebuje.isEmpty())) {
            prepareDatabase();

            //DateFormat df = new SimpleDateFormat("yyyy MM dd");
            //date = df.format(Calendar.getInstance().getTime());
            if (!dateFromCalendar.equals("")) {
                Objednavka objednavka = new Objednavka(userEmail, dateFromCalendar, uradId);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                // prepare call in Retrofit 2.0
                myApi = retrofit.create(RestApi.class);
                Call<Odpoved> call = myApi.createReservation(objednavka);
                call.enqueue(new Callback<Odpoved>() {
                    @Override
                    public void onResponse(Response<Odpoved> response, Retrofit retrofit) {
                        if (response.message().equals("Created")) {
                            //Toast.makeText(MainActivity.this, "Objednávka bola úspešne vytvorená.", Toast.LENGTH_LONG).show();


                        mydatabase.execSQL("INSERT INTO "+ RESERVATIONS_TABLE +" ("+TOKEN+","+REQUEST+","+DATETIME+","+URAD_ID+","+STATE+") VALUES('"+"beztokenu"+"','"
                                +chudacikPotrebuje+"','"+dateFromCalendar+"','"+uradId+"','2');");
                            Cursor res =  mydatabase.rawQuery( "SELECT "+RESERVATION_ID+" FROM "+RESERVATIONS_TABLE, null);
                            //System.out.println(res.getCount());
                            res.moveToFirst();
                            int resId = 0;
                            int i = 0;
                            while(res.isAfterLast() == false) {
                                resId = res.getInt(res.getColumnIndex(RESERVATION_ID));
                                System.out.println("RES ID "+resId);
                                i++;
                                res.moveToNext();
                            }
                            mydatabase.execSQL("INSERT INTO "+ REQUESTS_TABLE +" VALUES('"+resId+"','"
                                    +chudacikPotrebuje+"');");

                            updateReservationsDbs();
                            Toast.makeText(MainActivity.this, "Rezervácia bola úspešne vytvorená.", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(MainActivity.this, ReservationsActivity.class);
                            intent.putExtra(USER_EMAIL, userEmail);
                            intent.putExtra(USER_REQUEST, chudacikPotrebuje);
                            intent.putExtra(RESERVATIONS_NUMBER, pocetObj);
                            startActivity(intent);
                            //Cursor res = mydatabase.rawQuery("SELECT " + REQUEST + " FROM " + RESERVATIONS_TABLE, null);
                            //System.out.println("pocet OBJ " + res.getCount());
                        } else
                            Toast.makeText(MainActivity.this, "Na zvolený dátum už nie sú voľné termíny.", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Toast.makeText(MainActivity.this, "Nepodarilo sa vytvoriť objednávku. Skontrolujte internetové pripojenie.", Toast.LENGTH_LONG).show();
                        System.out.println(t.getLocalizedMessage());
                    }
                });
            } else Toast.makeText(MainActivity.this, "Vyberte dátum objednávky.", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Vyplň email a zvoľ čo chceš", Toast.LENGTH_SHORT).show();
        }


    }

    private void updateReservationsDbs() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        myApi = retrofit.create(RestApi.class);
        Call<List<Urad>> call = myApi.loadOffices();
        call.enqueue(new Callback<List<Urad>>() {
            @Override
            public void onResponse(Response<List<Urad>> response, Retrofit retrofit) {
                //mydatabase.execSQL("DROP TABLE IF EXISTS "+RESERVATIONS_TABLE);
                /*mydatabase.execSQL("CREATE TABLE IF NOT EXISTS " + RESERVATIONS_TABLE + "(" + TOKEN + " VARCHAR, "+REQUEST+" VARCHAR,"
                        + DATETIME + " VARCHAR," + URAD_ID + " INTEGER," + STATE + " INTEGER);");*/
                for (int i = 0; i < response.body().size(); i++) {
                    for(int j = 0; j < response.body().get(i).getObjednavkas().size(); j++) {
                    String token = response.body().get(i).getObjednavkas().get(j).getToken();
                    String dateTime = response.body().get(i).getObjednavkas().get(j).getDateTime();
                    int id = Integer.parseInt(response.body().get(i).getUradId());
                    String mail = response.body().get(i).getObjednavkas().get(j).getEmail();
                        if(mail.equals(userEmail)) {
                            /*mydatabase.execSQL("INSERT INTO "+ RESERVATIONS_TABLE +" VALUES('"+token+"','"
                                    +chudacikPotrebuje+"','"+dateTime+"','"+id+"','2');");*/
                            mydatabase.execSQL("UPDATE "+ RESERVATIONS_TABLE +" SET "+TOKEN+"='"+token+"' WHERE "+URAD_ID+" = "+id+"");
                            mydatabase.execSQL("UPDATE "+ RESERVATIONS_TABLE +" SET "+DATETIME+"='"+dateTime+"' WHERE "+URAD_ID+" = "+id+"");
                            System.out.println("UPDATE "+token+" - "+dateTime);
                        }
                    }
                }
                /*Cursor res = mydatabase.rawQuery("SELECT * FROM " + RESERVATIONS_TABLE, null);
                System.out.println("UPDATE DBS - pocet obj " + res.getCount());*/

            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(MainActivity.this, "Update lokálnej databázy zlyhal.", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void prepareListData() {
        // Adding child data
        listDataHeader.add("Nahlásenie");
        listDataHeader.add("Odhlásenie");
        listDataHeader.add("Žiadosť");
        listDataHeader.add("Podanie");
        listDataHeader.add("Platenie");
        listDataHeader.add("Udelenie");
        listDataHeader.add("Vybavenie");
        listDataHeader.add("Vydávanie");
        listDataHeader.add("Poskytovanie");


        /*Úrad prace, sociálnych vecí a rodiny ID 1*/
        podanie.add(new Problem("Podávanie informácií o príjme fyzickej osoby s ťažkým zdravotným postihnutím", 1));

        /*Miestny urad ID 2*/
        platenie.add(new Problem("Platenie miestneho poplatku za komunálne odpady a drobné stavebné odpady", 2));
        platenie.add(new Problem("Platenie miestnych daní", 2));
        platenie.add(new Problem("Platenie ostatných poplatkov", 2));
        platenie.add(new Problem("Platenie pokút, úrokov a sankčných úrokov", 2));
        udelenie.add(new Problem("Udeľovanie individuálnej licencie na prevádzkovanie hazardných hier prostredníctvom výherných prístrojov", 2));
        udelenie.add(new Problem("Udelenie oprávnenia podnikateľovi", 2));
        udelenie.add(new Problem("Udelenie oprávnenia podnikateľovi na zastupovanie orgánu verejnej moci", 2));
        vybavenie.add(new Problem("Vybavovanie petícií", 2));
        vybavenie.add(new Problem("Vybavovanie sťažností a podnetov", 2));
        vydavanie.add(new Problem("Vydávanie parkovacej karty", 2));
        vydavanie.add(new Problem("Vydávanie rybárskeho lístku", 2));
        vydavanie.add(new Problem("Vydávanie voličského preukazu", 2));

        /**Okresný úrad ID 3*/
        nahlasenie.add(new Problem("Nahlásenie straty alebo odcudzenia tabuliek s evidenčným číslom vozidla", 3));
        nahlasenie.add(new Problem("Nahlasovanie nálezu osvedčenia o evidencii vozidla", 3));
        nahlasenie.add(new Problem("Nahlasovanie straty alebo odcudzenia občianskeho preukazu s čipom", 3));
        nahlasenie.add(new Problem("Nahlasovanie straty, alebo odcudzenia osvedčenia o evidencii vozidla", 3));
        odhlasenie.add(new Problem("Odhlásenie vozidla do cudziny", 3));
        odhlasenie.add(new Problem("Odhlasovanie z prechodného pobytu", 3));
        odhlasenie.add(new Problem("Odhlasovanie z trvalého pobytu z dôvodu presťahovania do zahraničia", 3));
        ziadost.add(new Problem("Žiadosť o cestovný pas", 3));
        ziadost.add(new Problem("Žiadosť o náhradný cestovný doklad", 3));
        ziadost.add(new Problem("Žiadosť o občiansky preukaz", 3));
        ziadost.add(new Problem("Žiadosť o poskytnutie finančnej pomoci", 3));
        ziadost.add(new Problem("Žiadosť o vodičský preukaz", 3));
        ziadost.add(new Problem("Žiadosť o výpis z registra trestov", 3));
        ziadost.add(new Problem("Žiadosť o vyradenie z evidencie exportérov", 3));
        ziadost.add(new Problem("Žiadosť o zmenu mena a priezviska", 3));
        podanie.add(new Problem("Podávanie oznámení o späťvzatí/upustení používania priezviska po rozvode", 3));
        vydavanie.add(new Problem("Vydávanie občianskeho preukazu s čipom", 3));
        vydavanie.add(new Problem("Vydávanie úmrtného listu s pôvodným menom", 3));
        vydavanie.add(new Problem("Vydávanie úradných výpisov (duplikátov) matričných dokladov", 3));

         /*Okresný súd ID 5*/
        podanie.add(new Problem("Podávanie návrhu (podnetu) na začatie konania o zhode", 5));
        podanie.add(new Problem("Podávanie návrhu na zápis, zmenu a výmaz údajov v obchodnom registri", 5));

         /*Ministerstvo zahraničných vecí a európskych záležitostí Slovenskej republiky ID 6*/
        vybavenie.add(new Problem("Vybavovanie dokladu na prepravu zbraní", 6));
        vybavenie.add(new Problem("Vybavovanie sprievodného listu na prepravu telesných pozostatkov", 6));

        /*Ministerstvo dopravy, výstavby a regionálneho rozvoja Slovenskej republiky ID 7*/
        vydavanie.add(new Problem("Vydanie osvedčenia o absolvovaní odbornej prípravy v cestnej doprave", 7));
        vydavanie.add(new Problem("Vydanie rozhodnutia o schválení jednotlivo dovezeného vozidla", 7));
        vydavanie.add(new Problem("Vydanie rozhodnutia o schválení prestavby vozidla", 7));
        vydavanie.add(new Problem("Vydanie rozhodnutia o typovom schválení vozidla", 7));

        /*Ministerstvo hospodárstva SR ID 8*/
        ziadost.add(new Problem("Žiadosť o vydanie potvrdenia o zapísaní do zoznamu energetických audítorov", 8));

        /* Ministerstvo kultúry Slovenskej republiky ID 9*/
        poskytovanie.add(new Problem("Poskytovanie dotácií na rekonštrukciu kultúrnych pamiatok", 9));
        poskytovanie.add(new Problem("Poskytovanie dotácií na kultúru", 9));

        /*Ministerstvo školstva, vedy, výskumu a športu Slovenskej republiky ID 10*/
        poskytovanie.add(new Problem("Poskytovanie základného sociálneho poradenstva", 10));
        poskytovanie.add(new Problem("Poskytovanie výpisu z registra poskytovateľov sociálnych služieb", 10));
        poskytovanie.add(new Problem("Poskytovanie súťažných podkladov pre verejnú súťaž", 10));
        poskytovanie.add(new Problem("Poskytovanie sociálnej služby v zariadení sociálnej starostlivosti", 10));
        poskytovanie.add(new Problem("Poskytovanie informácií v oblasti výchovy a vzdelávania", 10));
        poskytovanie.add(new Problem("Poskytovanie informácií o regióne", 10));
        poskytovanie.add(new Problem("Poskytovanie finančného príspevku na prevádzku sociálnej služby", 10));
        poskytovanie.add(new Problem("Poskytovanie dotácií na šport", 10));

        /** Daňový úrad Košice ID 11*/
        podanie.add(new Problem("Podávanie daňového priznania k dani z nehnuteľností", 11));
        podanie.add(new Problem("Podávanie daňového priznania k dani za nevýherné hracie prístroje", 11));
        podanie.add(new Problem("Podávanie daňového priznania k dani za predajné automaty", 11));
        podanie.add(new Problem("Podávanie daňového priznania k dani za psa", 11));
        vydavanie.add(new Problem("Vydávanie stanoviska k chovu nebezpečných živočíchov", 11));
        vydavanie.add(new Problem("Vydávanie stanoviska k zmene druhu pozemku", 11));
        podanie.add(new Problem("Podávanie daňových priznaní", 11));
        podanie.add(new Problem("Podávanie daňových priznaní právnickej osoby ", 11));
        podanie.add(new Problem("Podávanie digitalizovaných podporných dokumentov do informačného systému Centrálny elektronický priečinok", 11));
        podanie.add(new Problem("Podávanie dokumentov do informačného systému Centrálny elektronický priečinok cez elektronickú podateľňu", 11));
        podanie.add(new Problem("Podávanie opravných prostriedkov voči rozhodnutiam v colnom konaní", 11));
        podanie.add(new Problem("Podávanie opravných prostriedkov voči rozhodnutiu o vydaní licencie", 11));
        vydavanie.add(new Problem("Vydávanie licencií pre realizáciu zahraničnoobchodnej transakcie", 11));

        listDataChild.put(listDataHeader.get(0), nahlasenie);
        listDataChild.put(listDataHeader.get(1), odhlasenie);
        listDataChild.put(listDataHeader.get(2), ziadost);
        listDataChild.put(listDataHeader.get(3), podanie);
        listDataChild.put(listDataHeader.get(4), platenie);
        listDataChild.put(listDataHeader.get(5), udelenie);
        listDataChild.put(listDataHeader.get(6), vybavenie);
        listDataChild.put(listDataHeader.get(7), vydavanie);
        listDataChild.put(listDataHeader.get(8), poskytovanie);


            }
}
